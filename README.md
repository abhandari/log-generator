# Log Generator

Log Generator Application. Base code in CPP.

## Build
Command:
``` shell
docker build -t logger:1.0 .
```

## Run
Command:
``` shell
docker run --rm -it -e SPEED='1000' -e LOG_LENGTH='0' -e ADD_NON_SUPPORTED_LOG='1' -e SKIP_MANDATORY_FIELDS='1' -e HAS_DIFFERENT_JSON_LOG='0' -e  HAS_INVALID_JSON_LOG='0' -e USE_OPTIONAL_FIELD='0' -e USE_FLEXI_STR='0' logger:1.0
```

## Arguments
- `SPEED` is mSecs. Application will print log on standard output at this interval.
- `LOG_LENGTH` is legth of the log to be flushed out. Posible values are 0 (~128 bytes), 1 (~512 bytes) & 2 (~1024 bytes).
- `ADD_NON_SUPPORTED_LOG` is (0/1). 1 value will make application to print additional logs at every 5th log.
- `SKIP_MANDATORY_FIELDS` is (0/1). 1 value will skip class field on event 3rd log.
- `HAS_DIFFERENT_JSON_LOG` is (0/1). 1 value will add totally different json for every 11th log.
- `HAS_INVALID_JSON_LOG` (0/1). 1 value will add an invalid json on every 7th log.
- `USE_OPTIONAL_FIELD` (0/1). 1 value will add optional values in output (context & stacktrace)
- `USE_FLEXI_STR` (0/1). 1 value will generate different flexi labels on every alternate log. (ERROR/ERR, WARN/WARNING, MESSAGE/MSG, TIMESTAMP/STAMP etc)

## Deployment
```deployment.yaml``` file contains the latest Deployment definition, container image is stored in a public repo on Docker Hub.
