#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include <chrono>
#include <thread>

using namespace std;

class Logger{
private:
    int logSpeed;
    bool isAdditionalLog;
    bool hasMissingMandatoryFields;
    bool hasDifferentValidJson;
    bool hasInvlidJson;
    bool useOptionalFields;
    bool useFlexiStrLevels;
    string getCurrentTimeStamp(){
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [50];

        time (&rawtime);
        timeinfo = localtime (&rawtime);

        strftime (buffer,50,"%FT%TZ",timeinfo);
        //puts (buffer);
        return string(buffer);
    }


public:
    long long logCount;
    const int MAX_LOG_LEVELS = 5;
    enum logLevel {DEBUG = 0, INFO, WARN, ERROR, FATAL};
    enum logSize {SMALL = 0, MEDIUM, LONG}; //SMALL = 512 bytes, MEDIUM = 3 KB, LONG = 5 KB
    logSize logStrSize;
    string logLevelStr[FATAL+1] = {"DEBUG", "INFO", "WARN", "ERROR", "FATAL"};
    void setSpeed(int speed){logSpeed = speed;}
    void setLogStrSize(logSize size){logStrSize = size;}
    Logger(int speed = 10):logSpeed(speed){
        logCount = 0;
        logStrSize = logSize::SMALL;
        isAdditionalLog = false;
        hasMissingMandatoryFields = false;
        hasDifferentValidJson = false;
        hasInvlidJson = false;
        useOptionalFields = false;
        useFlexiStrLevels = false;
    }

    void setAdditionalLog(bool addi){isAdditionalLog = addi;}
    void setHasMissingMandatoryFields(bool missing){hasMissingMandatoryFields = missing;}
    void setHasDifferentValidJson(bool diffJson){hasDifferentValidJson = diffJson;}
    void setHasInvlidJson(bool isInvalidJson){hasInvlidJson = isInvalidJson;}
    void setUseOptionalFields(bool optionalFields){useOptionalFields = optionalFields;}
    void setFlexiStrLevels(bool flexiStrLevels){useFlexiStrLevels = flexiStrLevels;}

    string getDummyStackTrace(){
        stringstream s_stack;

        s_stack << "0# boost::stacktrace::basic_stacktrace<std::allocator<boost::stacktrace::frame> >::basic_stacktrace() at /usr/include/boost/stacktrace/stacktrace.hpp:129\\n";
        s_stack << "1# my_func_1(int) at /home/ciro/test/boost_stacktrace.cpp:18\\n";
        s_stack << "2# main at /home/ciro/test/boost_stacktrace.cpp:29 (discriminator 2)\\n";
        s_stack << "3# __libc_start_main in /lib/x86_64-linux-gnu/libc.so.6\\n";
        s_stack << "4# _start in ./boost_stacktrace.out";
        
        return s_stack.str();
    }

    string getLogLevelStr(logLevel level, bool needFlexiLevelStr){
        switch(level){
            case DEBUG:
                return logLevelStr[level];
            break;
            case INFO:
                return logLevelStr[level];
            break;
            case WARN:
                if(needFlexiLevelStr && (logCount % 2) == 0){
                    return "WARNING";
                }else{
                    return logLevelStr[level];
                }
            break;
            case ERROR:
                if(needFlexiLevelStr && (logCount % 2) == 0){
                    return "ERR";
                }else{
                    return logLevelStr[level];
                }
            break;
            case FATAL:
                return logLevelStr[level];
            break;
            default:
                return "UNKNOWN";
            break;
        }
    }

    string getFlexiString(string str, bool needFlexiLevelStr){
        if(!needFlexiLevelStr)
            return str;

        if(str.compare("timestamp") == 0){
            if((logCount % 2) == 0){
                return "time";
            }
        }else if(str.compare("message") == 0){
            if((logCount % 2) == 0){
                return "msg";
            }
        }
        
        return str;
    }
    
    void log(logLevel level, logSize size, string comp, string ctx, string cls, string threadStr, string logStr ){
        string timeStamp = getCurrentTimeStamp();
        string levelStr = getLogLevelStr(level, useFlexiStrLevels);
        
        std::stringstream ss;

        if(hasInvlidJson  && ((logCount % 7) == 0)){
            //ss << "{"; // blocking this to make json invalid
            ss << "\"field1\":\"" << "fieldOne" << "\"" << ",";
            //ss << "\"field2\":\"" << "fieldTwo" << "\"" << ","; // valid one
            ss << "\"field2:\"" << "fieldTwo" << "\"" << ",";
            ss << "\"field3\":\"" << "fieldThree" << "\"" << ",";
            ss << "\"field4\":\"" << "fieldFour" << "\"" << ",";
            ss << "\"field5\":\"" << "fieldFive" << "\"";
            ss << "}";
        } else if(hasDifferentValidJson && ((logCount % 11) == 0)){
            ss << "{";
            ss << "\"field1\":\"" << "fieldOne" << "\"" << ",";
            ss << "\"field2\":\"" << "fieldTwo" << "\"" << ",";
            ss << "\"field3\":\"" << "fieldThree" << "\"" << ",";
            ss << "\"field4\":\"" << "fieldFour" << "\"" << ",";
            ss << "\"field5\":\"" << "fieldFive" << "\"";
            ss << "}";
        }
        else{
            //put arbitrary formatted data into the stream
            ss << "{";
            ss << "\"" << getFlexiString("timestamp", true) << "\":\"" << timeStamp << "\"" << ",";
            ss << "\"level\":\"" << levelStr << "\"" << ",";
            ss << "\"component\":\"" << comp << "\"" << ",";
            if(useOptionalFields){
                ss << "\"context\":\"" << ctx << "\"" << ",";
                ss << "\"stacktrace\":\"" << getDummyStackTrace() << "\"" << ",";
            }

            if(!hasMissingMandatoryFields || ((logCount % 3) != 0))
                ss << "\"class\":\"" << cls << "\"" << ",";
            
            if(isAdditionalLog && ((logCount % 5) == 0))
	            ss << "\"additionalLog\": \"*** THIS IS AN ADDITIONAL LOG ***\"" << ",";

            ss << "\"Thread\":\"" << threadStr << "\"" << ",";
            ss << "\"" << getFlexiString("message", true) << "\":\"" << "logLineCount-" << logCount << " *** " << logStr << "\"";
            ss << "}";
        }

        //convert the stream buffer into a string
        string str = ss.str();
        cout << str << endl;
    }

    string getLogStr(logSize size){
        switch(size){
            case logSize::SMALL:
                return "This is a SMALL size log which is around 128 bytes of length. and now to make it 128 I am adding dummy characters of no meaning xxxx";
            
            case logSize::MEDIUM:
                return "This is a MEDIUM size log which is around 512 bytes of length. and now to make it 512 I am adding dummy characters of no meaning yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";
            
            case logSize::LONG:
                return "This is a LONG size all log which is around 1024 bztes of length. and now to make it 1024 I am adding dummz characters of no meaning zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            
            default:
                return "This is a default(small) size log which is around 128 bytes of length. and now to make it 128 I am adding dummy characters of no meaning xxxx";            
        }
    }
    
    void printLog(logLevel level){
        log(level, logStrSize, "mock_Logger", "parsing", "org.empirix.com.Parser", "ThisThread", getLogStr(logStrSize));
    }
    
    void generateLog(){
        while(true){
            srand(time(0));
            logCount++;
            //printLog((Logger::logLevel)((rand() % MAX_LOG_LEVELS) + 1));
            printLog((Logger::logLevel)((rand() % (MAX_LOG_LEVELS-1))));

            std::this_thread::sleep_for(std::chrono::milliseconds(logSpeed));
            //if(logCount > 50) break;
        }
    }
};

int main(int argc, char **argv){
    cout << "You have entered " << argc
         << " arguments:" << "\n";

    for (int i = 0; i < argc; ++i)
        cout << argv[i] << "\n";
   
    Logger l;
    if(argc > 1){
        l.setSpeed(atoi(argv[1]));
        if(argc > 2)
            l.setLogStrSize((Logger::logSize)atoi(argv[2]));
        if(argc > 3)
            l.setAdditionalLog((bool)atoi(argv[3]));
        if(argc > 4)
            l.setHasMissingMandatoryFields((bool)atoi(argv[4]));
        if(argc > 5)
            l.setHasDifferentValidJson((bool)atoi(argv[5]));
        if(argc > 6)
            l.setHasInvlidJson((bool)atoi(argv[6]));
        if(argc > 7)
            l.setUseOptionalFields((bool)atoi(argv[7]));
        if(argc > 8)
            l.setFlexiStrLevels((bool)atoi(argv[8]));

        l.generateLog();
    }
}
