public class Logger{
private:
    int logSpeed;
    bool isAdditionalLog;
    String getCurrentTimeStamp(){
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [50];

        time (&rawtime);
        timeinfo = localtime (&rawtime);

        strftime (buffer,50,"%FT%TZ",timeinfo);
        //puts (buffer);
        return string(buffer);
    }


public:
    long long logCount;
    const int MAX_LOG_LEVELS = 5;
    enum logLevel {DEBUG = 0, INFO, WARN, ERROR, FATAL};
    enum logSize {SMALL = 0, MEDIUM, LONG}; //SMALL = 512 bytes, MEDIUM = 3 KB, LONG = 5 KB
    logSize logStrSize;
    string logLevelStr[FATAL+1] = {"DEBUG", "INFO", "WARN", "ERROR", "FATAL"};
    void setSpeed(int speed){logSpeed = speed;}
    void setLogStrSize(logSize size){logStrSize = size;}
    Logger(int speed = 10):logSpeed(speed){
        logCount = 0;
        logStrSize = logSize::SMALL;
        isAdditionalLog = false;
    }

    void setAdditionalLog(bool addi){isAdditionalLog = addi;}
    
    void log(logLevel level, logSize size, string comp, string ctx, string cls, string threadStr, string logStr ){
        string timeStamp = getCurrentTimeStamp();
        string levelStr = logLevelStr[level];
        
        std::stringstream ss;

        //put arbitrary formatted data into the stream
        ss << "{";
        ss << "\"timestamp\":\"" << timeStamp << "\"" << ",";
        ss << "\"level\":\"" << levelStr << "\"" << ",";
        ss << "\"component\":\"" << comp << "\"" << ",";
        ss << "\"context\":\"" << ctx << "\"" << ",";
        ss << "\"class\":\"" << cls << "\"" << ",";
        
        if(isAdditionalLog && ((logCount % 5) == 0))
           ss << "\"additionalLog\": \"ADDITIONAL LOG IS HERE.... THIS IS UNSUPPORTED LOG\"" << ",";

        ss << "\"Thread\":\"" << threadStr << "\"" << ",";
        ss << "\"detail\":\"" << "logLineCount-" << logCount << " *** " << logStr << "\"";
        ss << "}";

        //convert the stream buffer into a string
        string str = ss.str();
        cout << str << endl;
    }

    string getLogStr(logSize size){
        switch(size){
            case logSize::SMALL:
                return "This is a SMALL size log which is around 128 bytes of length. and now to make it 128 I am adding dummy characters of no meaning xxxx";
            
            case logSize::MEDIUM:
                return "This is a MEDIUM size log which is around 512 bytes of length. and now to make it 512 I am adding dummy characters of no meaning yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";
            
            case logSize::LONG:
                return "This is a LONG size all log which is around 1024 bztes of length. and now to make it 1024 I am adding dummz characters of no meaning zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            
            default:
                return "This is a default(small) size log which is around 128 bytes of length. and now to make it 128 I am adding dummy characters of no meaning xxxx";            
        }
    }
    
    void printLog(logLevel level){
        log(level, logStrSize, "mock_Logger", "parsing", "org.empirix.com.Parser", "ThisThread", getLogStr(logStrSize));
    }
    
    void generateLog(){
        while(true){
            srand(time(0));
            logCount++;
            //printLog((Logger::logLevel)((rand() % MAX_LOG_LEVELS) + 1));
            printLog((Logger::logLevel)((rand() % (MAX_LOG_LEVELS-1))));

            std::this_thread::sleep_for(std::chrono::milliseconds(logSpeed));
            //if(logCount > 50) break;
        }
    }

    public static void main(String[] args){
        cout << "You have entered " << argc
         << " arguments:" << "\n";

    for (int i = 0; i < argc; ++i)
        cout << argv[i] << "\n";

    Logger l;
    if(argc > 1){
        l.setSpeed(atoi(argv[1]));
        if(argc > 2)
            l.setLogStrSize((Logger::logSize)atoi(argv[2]));
        if(argc > 3)
            l.setAdditionalLog((bool)atoi(argv[3]));

        l.generateLog();

    }
}
